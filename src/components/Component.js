class Component {
  tagName;
  children;
  attribute;

  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.children = children;
    this.attribute = attribute;
  }
  renderAttribute() {
    let attributes = "";
    if (this.attribute)
      attributes = `${this.attribute.name}="${this.attribute.value}"`;

    return attributes;
  }
  render() {
    if (this.children) {
      return `<${
        this.tagName
      } ${this.renderAttribute()}>${this.renderChildren()}</${this.tagName}>`;
    }
    return `<${this.tagName} ${this.renderAttribute()}/>`;
  }
  renderChildren() {
    if (this.children instanceof Array) {
      let arr = this.children.map((child) => {
        if (child instanceof Component) {
          return child.render();
        } else {
          return `${child}`;
        }
      });
      return arr.join("");
    }
    return this.children;
  }
}

export default Component;
