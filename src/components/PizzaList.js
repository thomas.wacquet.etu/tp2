import PizzaThumbnail from "./PizzaThumbnail";

import Component from "./Component";
export default class PizzaList {
  #pizzas;
  set pizzas(value) {
    this.#pizzas = value;
  }
  constructor(data) {
    this.pizzas = data;
  }
  render() {
    return `<section class="pizzaList">${this.#pizzas
      .map((pizza) => {
        return new PizzaThumbnail(pizza).render();
      })
      .join("")}</section>`;
  }
}
