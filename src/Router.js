import Component from "./components/Component";

export default class Router {
  static titleElement;
  static contentElement;
  static routes;

  static navigate(path) {
    Router.routes.forEach((route) => {
      if (route.path == path) {
        Router.titleElement.innerHTML = new Component(
          "h1",
          null,
          route.title
        ).render();
        Router.contentElement.innerHTML = route.page.render();
      }
    });
  }
}
