import Component from "./components/Component.js";
import Img from "./components/Img.js";
import { data } from "./data.js";
import PizzaThumbnail from "./components/PizzaThumbnail";
import PizzaList from "./components/PizzaList";
import Router from "./Router";

Router.titleElement = document.querySelector(".pageTitle");
Router.contentElement = document.querySelector(".pageContent");
const pizzaList = new PizzaList([]);
Router.routes = [{ path: "/", page: pizzaList, title: "La carte" }];

Router.navigate("/"); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate("/");
